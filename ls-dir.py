'''
$ python --version
Python 3.6.0 :: Anaconda custom (64-bit)

Attempting to do ```$ ls -Al``` using python - os.scandir() (no subprocess or similar modules
to execute "ls -l" and then retrieve the output from the system)


permissions hardlinks owner_name group_name file_size month_mod  date_mod year_mod/time_mod file_name
'''

import os, sys
import grp, pwd
import datetime


def get_time_str(val):
    return ('0'+str(val)) if (len(str(val)) == 1) else str(val)

def get_perm_str(perm_code, dir_str):
    dic = {'7':'rwx', '6' :'rw-', '5' : 'r-x', '4':'r--', '0': '---'}
    perm = str(oct(perm_code)[-3:])  ## discarded bits are for the file type
    return dir_str + ''.join(dic.get(x,x) for x in perm)

def ls_(statRes, f_name, dir_token):
    #print(dir(statRes))
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    date_time = datetime.datetime.fromtimestamp(statRes.st_mtime)
    #print(date_time)
    year_or_time = get_time_str(date_time.hour) +':'+ get_time_str(date_time.minute)
    if datetime.datetime.today().year > date_time.year:
        year_or_time = date_time.year
    permStr = get_perm_str(statRes.st_mode, dir_token)
    print("{} {} {} {} {} {}  {} {} {}".format(permStr, statRes.st_nlink,
                             pwd.getpwuid(statRes.st_uid)[0],
                             grp.getgrgid(statRes.st_gid)[0], statRes.st_size, 
                             months[date_time.month - 1], date_time.day, year_or_time,
                             f_name)
          )


if __name__ == "__main__":
    pathStr = '.'  # use '.' as default
    ##if sys.argv[1] != '':
        ##pathStr = sys.argv[1]
    ##print(sys.argv)
    with os.scandir(pathStr) as dirItems:
        dirToken = '-'
        for f_item in dirItems:
            if f_item.is_dir():
                dirToken = 'd'
	    #print("name: {}".format(f_item.name))
	    #print("path: {}".format(f_item.path))
	    #print("is_dir: {}".format(f_item.is_dir()))
	    #print("is_file: {}".format(f_item.is_file()))
            ls_(os.stat(f_item.name), f_item.name, dirToken)





''' Later =>
i) accept path work as command-line argument
ii) test a bit more thoroughly
iii) show all file types apppropriately
'''

